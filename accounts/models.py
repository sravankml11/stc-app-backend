import uuid

from base.models import AbstractModel
# from choicesenum.django.fields import EnumIntegerField
from django.contrib.auth.models import AbstractUser
from django.db import models
from django_enum_choices.fields import EnumChoiceField
from stcapp.choice import Class, Division

# Create your models here.


class User(AbstractUser):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    school = models.ForeignKey('accounts.School', related_name='school', on_delete=models.CASCADE)
    is_teacher = models.BooleanField(default=True)


class ClassName(AbstractModel):
    name = EnumChoiceField(Class)
    division = EnumChoiceField(Division)

    def __str__(self):
        return f'{self.name.value}-{self.division.value}'


class School(AbstractModel):
    name = models.CharField(max_length=250)
    sub_domain = models.ForeignKey('clients.Domain', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.name)
