# from stcapp.models import School
from accounts.models import School, User
from clients.models import Domain
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from stcapp.models import ProfileDetails, Teachers


class DomainSerializers(serializers.ModelSerializer):
    class Meta:
        model = Domain
        fields = '__all__'


class SchoolSerializers(serializers.ModelSerializer):
    sub_domain = DomainSerializers()

    class Meta:
        model = School
        fields = '__all__'


class UserSerializers(serializers.ModelSerializer):
    school = SchoolSerializers()

    class Meta:
        model = User
        fields = '__all__'


class ProfileDetailsSerializers(serializers.ModelSerializer):
    profile = UserSerializers()
    class_now = serializers.SerializerMethodField()
    teacher = serializers.SerializerMethodField()
    subject_name = serializers.SerializerMethodField()

    def get_class_now(self, obj):
        return f'{obj.class_name.get_name_display()}-{obj.class_name.get_division_display()}'

    def get_teacher(self, obj):
        teacher = Teachers.objects.filter(user=obj.profile).first()
        if teacher:
            return teacher.subject.subject_choice.value
        else:
            return None

    def get_subject_name(self, obj):
        teacher = Teachers.objects.filter(user=obj.profile).first()
        if teacher:
            return teacher.subject.name
        else:
            return None

    class Meta:
        model = ProfileDetails
        fields = '__all__'


class ObtainSerializedToken(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['domain'] = self.user.school.sub_domain.domain
        return data
