from accounts.api_v1.serializer import (ObtainSerializedToken,
                                        ProfileDetailsSerializers,
                                        UserSerializers)
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenViewBase
from stcapp.models import ProfileDetails


class ObtainTokenView(TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = ObtainSerializedToken


class ProfileDetailsView(ListAPIView):
    serializer_class = ProfileDetailsSerializers

    def get_queryset(self):
        user = ProfileDetails.objects.filter(profile=self.request.user)
        return user


class UserDetailsView(APIView):

    def get(self, request):
        user = request.user
        user_data = UserSerializers(user)
        return Response(user_data.data)
