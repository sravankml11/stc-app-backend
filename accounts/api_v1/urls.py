from accounts.api_v1.view import ProfileDetailsView, UserDetailsView
from django.urls import path

urlpatterns = [
    path('profile/details/', ProfileDetailsView.as_view(), name='profile-details'),
    path('user/details/', UserDetailsView.as_view(), name='user-details'),
]
