import enum

# Using enum class create enumerations


class Class(enum.Enum):
    ONE = 'I'
    TWO = 'II'
    THREE = 'III'
    FOUR = 'IV'
    FIVE = 'V'
    SIX = 'VI'
    SEVEN = 'VII'
    EIGHT = 'VIII'
    NINE = 'IX'
    TEN = 'X'
    ELEVEN = 'XI'
    TWELVE = 'XII'


class Division(enum.Enum):
    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'
    E = 'E'
    F = 'F'
    G = 'G'
    H = 'H'
    I = 'I'
