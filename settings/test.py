from django.conf import settings

from settings.common import *
from settings.common import INSTALLED_APPS, os

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'stcapp1',
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': 'stca_databse_1',
        'PORT': os.environ.get('DB_PORT'),
    }
}
