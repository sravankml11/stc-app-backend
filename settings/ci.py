from django.conf import settings

from settings.common import *
from settings.common import INSTALLED_APPS, os

DATABASES = {
    'default': {
       'ENGINE': 'django_tenants.postgresql_backend',
       'NAME': 'ci',
       'USER': 'postgres',
       'PASSWORD': 'postgres',
       'HOST': 'postgres',
       'PORT': '5432',
    }
}
