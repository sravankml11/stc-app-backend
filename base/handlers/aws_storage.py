import boto3
from botocore.client import Config


class AWSClient():
    def __init__(self, service):
        self.s3_client = boto3.client(service, config=Config(signature_version='s3v4'), region_name='ap-south-1')

    def upload_file(self, file, bucket, object_name):
        response = self.s3_client.upload_fileobj(file, bucket, object_name)
        return response

    def get_object_url(self, key, bucket):
        url = self.s3_client.generate_presigned_url(ClientMethod='get_object',
                                                    Params={'Bucket': bucket,
                                                            'Key': key},
                                                    HttpMethod="GET",
                                                    ExpiresIn=10800,)
        return url

    def delete_object(self, key, bucket):
        response = self.s3_client.delete_object(Bucket=bucket, Key=key)
        return response
