from base.models import AbstractModel
from django.db import models
from django_tenants.models import DomainMixin, TenantMixin


class Client(AbstractModel, TenantMixin):
    name = models.CharField(max_length=100)
    on_trial = models.BooleanField()
    created_on = models.DateField(auto_now_add=True)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True


class Domain(AbstractModel, DomainMixin):
    pass

    def __str__(self):
        return f'{self.domain}'
