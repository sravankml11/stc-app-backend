from accounts.models import User
from base.models import AbstractModel
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models


# Create your models here.
class MCQTest(AbstractModel):
    name = models.CharField(max_length=200, null=True, blank=True)
    chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Question(AbstractModel):
    description = RichTextUploadingField()
    mcq_test = models.ForeignKey(MCQTest, on_delete=models.CASCADE)

    def __str__(self):
        return self.description


class Choices(AbstractModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    description = RichTextUploadingField()
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.description


class TestScore(AbstractModel):
    mcq_test = models.ForeignKey(MCQTest, on_delete=models.CASCADE)
    score = models.IntegerField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return '{self.mcq_test}-{self.user}-{self.user}'
