from django.contrib import admin

from mcq.models import Choices, MCQTest, Question, TestScore

# Register your models here.
admin.site.register(MCQTest)
admin.site.register(Question)
admin.site.register(Choices)
admin.site.register(TestScore)
