# Generated by Django 2.2 on 2021-11-10 13:52

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('lesson_reading', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentLessonRecordings',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('file_key', models.CharField(max_length=250)),
                ('student_reading_content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lesson_reading.StudentReadingContent')),
            ],
            options={
                'ordering': ['-created_at'],
                'abstract': False,
            },
        ),
    ]
