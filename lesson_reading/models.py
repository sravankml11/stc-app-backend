from base.models import AbstractModel
from django.db import models

# Create your models here.


class ChapterReadingContent(AbstractModel):
    content = models.TextField()
    class_name = models.ManyToManyField('accounts.ClassName')
    subject = models.ForeignKey('stcapp.Subject', on_delete=models.CASCADE)
    chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.subject}-{self.class_name}-{str(self.chapter)}'


class StudentReadingContent(AbstractModel):
    content = models.TextField()
    chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)
    user_details = models.ForeignKey('stcapp.ProfileDetails', on_delete=models.CASCADE)
    reading_percentage = models.CharField(default=0, max_length=5)

    def __str__(self):
        return f'{self.user_details}-{str(self.chapter)}'


class StudentLessonRecordings(AbstractModel):
    file_key = models.CharField(max_length=250)
    # student_reading_content = models.ForeignKey('lesson_reading.StudentReadingContent', on_delete=models.CASCADE)
    chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)
    user_details = models.ForeignKey('stcapp.ProfileDetails', on_delete=models.CASCADE)
    duration = models.IntegerField()

    def __str__(self):
        return f'{self.file_key}-{str(self.chapter)}'
