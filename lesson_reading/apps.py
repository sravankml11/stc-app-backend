from django.apps import AppConfig


class LessonReadingConfig(AppConfig):
    name = 'lesson_reading'
