import boto3


class AWSClient():
    def __init__(self, service):
        self.s3_client = boto3.client(service)

    def upload_file(self, file, bucket, object_name):
        response = self.s3_client.upload_fileobj(file, bucket, object_name)
        return response
