from django.contrib import admin

from lesson_reading.models import (ChapterReadingContent,
                                   StudentLessonRecordings,
                                   StudentReadingContent)

# Register your models here.
admin.site.register(ChapterReadingContent)
admin.site.register(StudentReadingContent)
admin.site.register(StudentLessonRecordings)
