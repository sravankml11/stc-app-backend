# import itertools
import datetime as dt
from datetime import datetime

# import gensim
from base.constant import S3BucketName
from base.handlers.aws_storage import AWSClient
from django.db.models import Max, Sum
# from lesson_reading.api_v1.serializer import \
#     GerStudentContentReadingSerializers
from lesson_reading.models import \
    StudentLessonRecordings  # ChapterReadingContent,; StudentReadingContent
# from nltk.tokenize import word_tokenize
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from stcapp.models import Chapter


class GetUserReadingContent(APIView):
    def get(self, request):
        chapter_id = request.GET.get('chapterId')
        studentId = request.GET.get('studentId')
        if studentId:
            reading_audio_key = StudentLessonRecordings.objects.filter(chapter__uuid=chapter_id,
                                                                       user_details__uuid=studentId
                                                                       ).order_by('created_at')
        else:
            reading_audio_key = StudentLessonRecordings.objects.filter(chapter__uuid=chapter_id,
                                                                       user_details=request.user.profile
                                                                       ).order_by('created_at')
        audio_list = []
        for file in reading_audio_key:
            file_url = AWSClient('s3').get_object_url(file.file_key, S3BucketName.lesson_recordings)
            audio_list.append({"url": file_url, "uuid": str(file.uuid)})
        print(audio_list)
        return Response(audio_list)


class SubmitUserReadingContent(APIView):
    # def format_dict(self, text):
    #     text_doc = [w.lower() for w in word_tokenize(text)]
    #     text_dictionary_gensim = gensim.corpora.Dictionary([text_doc])
    #     text_dictionary = text_dictionary_gensim.token2id
    #     char = ['!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<',
    #             '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~', '“', '”', '—', '!', '’']
    #     for ch in char:
    #         get_val = text_dictionary.get(ch)
    #         if get_val or get_val == 0:
    #             print(get_val)
    #             del text_dictionary[ch]
    #     # print(text_dictionary)
    #     text_len = len(text_dictionary)
    #     print('text lenght', text_len)
    #     corpus = text_dictionary_gensim.doc2bow(text_doc)
    #     new_dict = {}
    #     for item, item2 in itertools.zip_longest(text_dictionary, corpus):
    #         new_dict[item] = item2[1]
    #     return new_dict

    def post(self, request):
        chapter_id = request.POST.get('chapterId')
        duration = request.POST.get('duration')
        file = request.data.get('file')
        date = datetime.now()
        chapter = Chapter.objects.get(uuid=chapter_id)
        class_name = request.user.profile.class_name.get_name_display()
        username = request.user.username
        subject = chapter.subject.name
        chapter_name = chapter.name
        object_file_name = f'{request.tenant.name}/{date.year}/{class_name}/{username}/{subject}/{chapter_name}/{file.name}'  # noqa

        client = AWSClient('s3')
        client.upload_file(file, S3BucketName.lesson_recordings, object_file_name)
        student_recording = StudentLessonRecordings.objects.create(file_key=object_file_name, chapter=chapter,
                                                                   user_details=request.user.profile,
                                                                   duration=duration)
        recroding_url = AWSClient('s3').get_object_url(student_recording.file_key, S3BucketName.lesson_recordings)
        data = {"url": recroding_url, "uuid": str(student_recording.uuid)}
        # content = request.POST
        # chapter = Chapter.objects.get(uuid=chapter_id)
        # user_content, created = StudentReadingContent.objects.update_or_create(
        #     user_details=request.user.profile,
        #     chapter=chapter, defaults={'content': content})
        # chapter_content = ChapterReadingContent.objects.get(chapter__uuid=chapter_id)
        # user_content_dict = self.format_dict(user_content.content)
        # chapter_content_dict = self.format_dict(chapter_content.content)

        # chapter_conten_val = 0
        # for key, val in chapter_content_dict.items():
        #     chapter_conten_val += val

        # user_content_val = 0
        # for key, val in user_content_dict.items():
        #     dict1_val = chapter_content_dict.get(key)
        #     if dict1_val:
        #         if val >= dict1_val:
        #             val = dict1_val
        #         user_content_val += val

        # user_chapter_val = (user_content_val * 100) / chapter_conten_val

        # chapter_compleated = user_chapter_val / 100
        # rounded_val = round(chapter_compleated, 2)
        # user_content.reading_percentage = rounded_val
        # user_content.save()
        return Response(data, status=status.HTTP_202_ACCEPTED)


class DeleteRecording(APIView):

    def delete(self, request, uuid):
        recording_uuid = uuid
        object = StudentLessonRecordings.objects.get(uuid=recording_uuid)
        AWSClient('s3').delete_object(object.file_key, S3BucketName.lesson_recordings)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GetStudentReadingList(APIView):
    def get(self, request):
        chapter = self.request.GET.get('chapterId')
        class_id = self.request.GET.get('classId')
        reading_list = StudentLessonRecordings.objects.filter(
            chapter__uuid=chapter, user_details__class_name__uuid=class_id)
        data = reading_list.values('user_details__profile__username', 'chapter', 'user_details').annotate(
            Sum('duration'), last_read=Max('created_at')).order_by()
        return Response(data, status=status.HTTP_202_ACCEPTED)


class GetWeeklyStats(APIView):
    def get(self, request):
        user_details = request.user.profile
        my_date = dt.date.today()
        year, week_num, day_of_week = my_date.isocalendar()
        readings = StudentLessonRecordings.objects.filter(user_details=user_details, created_at__week=week_num)
        stats = readings.values('chapter__subject__name', 'chapter__name').annotate(
            Sum('duration')).order_by()
        return Response(stats, status=status.HTTP_202_ACCEPTED)
