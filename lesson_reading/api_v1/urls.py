from django.urls import path
from lesson_reading.api_v1.views import (DeleteRecording,
                                         GetStudentReadingList,
                                         GetUserReadingContent, GetWeeklyStats,
                                         SubmitUserReadingContent)

urlpatterns = [
    path('get/student/reading/content/', GetUserReadingContent.as_view(), name='student-reading-content'),
    path('submit/student/lesson/reading/', SubmitUserReadingContent.as_view(), name='student-reading-submit'),
    path('student/recording/delete/<uuid:uuid>/', DeleteRecording.as_view(), name='student-recording-delete'),
    path('student/reading/list/get/', GetStudentReadingList.as_view(), name='student-reading-list'),
    path('student/reading/weekly/stats/', GetWeeklyStats.as_view(), name='student-reading-weekly')
]
