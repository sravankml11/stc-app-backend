
from lesson_reading.models import StudentReadingContent
from rest_framework import serializers


class GerStudentContentReadingSerializers(serializers.ModelSerializer):
    class Meta:
        model = StudentReadingContent
        fields = '__all__'
