from django.urls import path
from stcapp.api_v2.views import TopicViewAPIV2

urlpatterns = [
    path('chapter/topic/list/', TopicViewAPIV2.as_view(), name='topic-list-v2'),
]
