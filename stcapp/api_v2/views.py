# import pdb

# from django.db import models
from django.db.models import Sum
from lesson_reading.models import \
    StudentLessonRecordings  # StudentReadingContent
from rest_framework.response import Response
from rest_framework.views import APIView
from stcapp.api_v1.serializer import TopicSerializer
from stcapp.models import Chapter, Topic


class TopicViewAPIV2(APIView):

    def get(self, request):
        chapter_id = request.GET.get('chapter_id')
        user_details = request.user.profile
        chapter = Chapter.objects.get(uuid=chapter_id)
        # user_content = StudentReadingContent.objects.filter(
        #     user_details=user_details,
        #     chapter__uuid=chapter_id).first()
        # if user_content:
        #     user_reading_percentage = user_content.reading_percentage
        # else:
        #     user_reading_percentage = 0
        lesson_readings = StudentLessonRecordings.objects.filter(
            chapter__uuid=chapter_id, user_details=user_details).order_by('created_at')
        duration = 0
        last_read = ''
        if lesson_readings:
            duration_ms = lesson_readings.aggregate(Sum('duration')).get('duration__sum')
            duration = round(duration_ms / 60000)
            last_read = lesson_readings.last().created_at

        topics = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False,
                                      is_accessible=True).order_by('order_no')
        topics_data = TopicSerializer(topics, many=True).data
        data = {"topics": topics_data,
                "duration": duration,
                'last_read': last_read,
                'chapter_name': chapter.name}
        return Response(data)
