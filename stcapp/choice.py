import enum

from choicesenum import ChoicesEnum

# Using enum class create enumerations


class Class(enum.Enum):
    ONE = 'I'
    TWO = 'II'
    THREE = 'III'
    FOUR = 'IV'
    FIVE = 'V'
    SIX = 'VI'
    SEVEN = 'VII'
    EIGHT = 'VIII'
    NINE = 'IX'
    TEN = 'X'
    ELEVEN = 'XI'
    TWELVE = 'XII'


class Division(enum.Enum):
    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'
    E = 'E'
    F = 'F'
    G = 'G'
    H = 'H'
    I = 'I'


class SubjectChoice(ChoicesEnum):
    Mathematics = 10
    Physics = 20
    Chemistry = 30
    Biology = 40
    History = 50
    Geography = 60
    Politics = 70
    Computer = 80
    English = 90
    Hindi = 100
    Malayalam = 110
    Kannada = 120
