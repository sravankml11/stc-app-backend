from django import forms

from stcapp.models import Question, Topic  # Exercise,


class ContactForm(forms.ModelForm):
    pass


class QuestionAnswerForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = '__all__'


class TopicCreateForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = '__all__'


# class ExerciseCreateForm(forms.ModelForm):
#     class Meta:
#         model = Exercise
#         fields = '__all__'
