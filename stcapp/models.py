from accounts.models import User
from base.models import AbstractModel
from choicesenum.django.fields import EnumIntegerField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from stcapp.choice import SubjectChoice


class Topic(AbstractModel):
    chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)
    topic_no = models.CharField(max_length=50, null=True, blank=True)
    video_title = models.CharField(max_length=200, null=True, blank=True)
    name = RichTextUploadingField(null=True, blank=True)
    order_no = models.DecimalField(max_digits=5, decimal_places=2)
    topic = models.ForeignKey('self', related_name='sub_topic', limit_choices_to={
                              'is_sub_topic': False}, on_delete=models.CASCADE, null=True, blank=True)
    is_sub_topic = models.BooleanField('sub topic/exercise', default=False)
    is_exercise = models.BooleanField(default=False)
    is_accessible = models.BooleanField(default=True)

    def __str__(self):
        return '{}-{}'.format(self.chapter.chapter_no, self.name)


class ProfileDetails(AbstractModel):
    profile = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    profile_photo = models.ImageField(upload_to='profile_pic', null=True, blank=True)
    class_name = models.ForeignKey('accounts.ClassName', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.profile.username)


class Subject(AbstractModel):
    name = models.CharField(max_length=100)
    subject_code = models.CharField(max_length=50)
    image = models.ImageField(upload_to='images/subject', null=True, blank=True)
    subject_choice = EnumIntegerField(enum=SubjectChoice,
                                      default=SubjectChoice.Mathematics)

    def __str__(self):
        return '{}'.format(self.name)


class Teachers(AbstractModel):
    name = models.CharField(max_length=200)
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    subject = models.ForeignKey('stcapp.Subject', on_delete=models.CASCADE)
    class_name = models.ForeignKey('accounts.ClassName', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    folder_id = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Chapter(AbstractModel):
    name = models.CharField(max_length=200)
    chapter_no = models.IntegerField()
    subject = models.ForeignKey('stcapp.Subject', on_delete=models.CASCADE)
    class_name = models.ManyToManyField('accounts.ClassName')

    class Meta:
        ordering = ['chapter_no']

    def __str__(self):
        return '{}-{}-{}'.format(self.subject.name, self.name, self.chapter_no)


# class Exercise(AbstractModel):
#     chapter = models.ForeignKey('Chapter', on_delete=models.CASCADE)
#     exercise_no = models.CharField(max_length=50)
#     video_title = models.CharField(max_length=200, null=True, blank=True)
#     name = RichTextUploadingField(null=True, blank=True)
#     order_no = models.DecimalField(max_digits=5, decimal_places=2)
#     exercise = models.ForeignKey('self', related_name="sub_exercise",
#     on_delete=models.CASCADE, null=True, blank=True)
#     is_sub_exercise = models.BooleanField(default=False)

#     def __str__(self):
#         return '{}-{}-{}'.format(self.chapter.chapter_no, self.exercise_no, self.name)


class Question(AbstractModel):
    # exercise = models.ForeignKey('Exercise', on_delete=models.CASCADE, null=True, blank=True)
    topic = models.OneToOneField(Topic, on_delete=models.CASCADE, null=True, blank=True)
    question_no = models.CharField(max_length=50, null=True, blank=True)
    content = RichTextUploadingField(null=True, blank=True)
    video_key = models.CharField(max_length=300, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        # try:
        name = self.topic.name
        no = self.topic.topic_no
        # except AttributeError:
        #     name = self.exercise.chapter.name
        #     no = f'Exe {self.exercise.exercise_no} - {self.question_no}'

        return '{}-{}'.format(name, no)


class VideoAction(AbstractModel):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    topic = models.ForeignKey('Topic', on_delete=models.CASCADE)
    got_it = models.BooleanField(default=False)
    not_clear = models.BooleanField(default=False)
    check_later = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user}-{str(self.topic)}'


class VideoWatched(AbstractModel):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    topic = models.ForeignKey('Topic', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}-{str(self.topic)}'


# class ChapterReadingContent(AbstractModel):
#     content = models.TextField()
#     class_name = models.ManyToManyField('accounts.ClassName')
#     subject = models.ForeignKey('stcapp.Subject', on_delete=models.CASCADE)
#     chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)

#     def __str__(self):
#         return f'{self.subject}-{self.class_name}-{str(self.chapter)}'


# class StudentReadingContent(AbstractModel):
#     content = models.TextField()
#     chapter = models.ForeignKey('stcapp.Chapter', on_delete=models.CASCADE)
#     user_details = models.ForeignKey('stcapp.ProfileDetails', on_delete=models.CASCADE)
#     reading_percentage = models.CharField(default=0, max_length=5)

#     def __str__(self):
#         return f'{self.user_details}-{str(self.chapter)}'
