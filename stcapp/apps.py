from django.apps import AppConfig


class StcappConfig(AppConfig):
    name = 'stcapp'
