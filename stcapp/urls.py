from django.urls import path

from stcapp.views import (ChapterCreateView, ChapterDeleteView,
                          ChaptersListView, ChapterUpdateView,
                          ExerciseCreateView, ExerciseUpdateView,
                          QuestionAnswer, SubjectListView, TopicCreateView,
                          TopicDeleteView, TopicListView, TopicUpdateView,
                          UploadVideo, ViewUploadedVideo)

urlpatterns = [
    path('class/<uuid:id>/subject/list/', SubjectListView.as_view(), name='subject-list'),
    path('class/<uuid:class_id>/subject/<uuid:subject_id>/chapters/list/',
         ChaptersListView.as_view(), name='chapter-list'),
    path('class/<uuid:class_id>/subject/<uuid:subject_id>/chapter/create',
         ChapterCreateView.as_view(), name='chapter-create'),
    path('class/<uuid:class_id>/subject/<uuid:subject_id>/chapter/<uuid:pk>/update/',
         ChapterUpdateView.as_view(), name='chapter-update'),
    path('class/<uuid:class_id>/subject/<uuid:subject_id>/chapter/<uuid:pk>/delete/',
         ChapterDeleteView.as_view(), name='chapter-delete'),
    path('chapters/<uuid:chapter_id>/topic/list/', TopicListView.as_view(), name='topic-list-view'),
    path('chapters/<uuid:chapter_id>/topic/create/', TopicCreateView.as_view(), name='topic-create'),
    path('chapters/<uuid:chapter_id>/topic/<uuid:topic_id>/edit/', TopicUpdateView.as_view(), name='topic-update'),
    path('chapters/<uuid:chapter_id>/topic/<uuid:pk>/delete/', TopicDeleteView.as_view(), name='topic-delete'),
    path('chapters/<uuid:chapter_id>/topic/<uuid:topic_id>/create/question-answer/',
         QuestionAnswer.as_view(), name='question-answer-create'),
    path('chapters/<uuid:chapter_id>/exercise/create/', ExerciseCreateView.as_view(), name='exercise-create'),
    path('chapters/<uuid:chapter_id>/exercise/<uuid:topic_id>/edit/',
         ExerciseUpdateView.as_view(), name='exercise-update'),
    path('upload/video/<uuid:topic_id>/', UploadVideo.as_view(), name='upload-video'),
    path('upload/video/view/<uuid:topic_id>/<str:folder_id>/', ViewUploadedVideo.as_view(), name='upload-video-view')
]
