# import pdb

from accounts.models import ClassName
from django.conf import settings
from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
# Create your views here.
# from django.utils import timezone
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView, View

from stcapp.forms import QuestionAnswerForm  # ExerciseCreateForm,
from stcapp.forms import TopicCreateForm
from stcapp.handler.manage_vimeo import ManageVimeo
from stcapp.models import (Chapter, Question, Subject, Teachers,  # Exercise,
                           Topic)

# from rest_framework.response import Response


class ChaptersListView(ListView):

    model = Chapter

    def get_queryset(self, **kwargs):
        queryset = Chapter.objects.filter(
            subject__uuid=self.kwargs['subject_id'], class_name=self.kwargs['class_id']).order_by('chapter_no')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subject_id'] = self.kwargs['subject_id']
        context['class_id'] = self.kwargs['class_id']
        return context


class ClassNameListView(ListView):

    model = ClassName
    paginate_by = 100  # if pagination is desired


class SubjectListView(ListView):

    model = Subject

    def get_queryset(self, **kwargs):
        class_id = self.kwargs['id']
        teachers = Teachers.objects.filter(class_name__uuid=class_id, active=True)
        subject_uuid = teachers.values_list('subject', flat=True)
        subject = Subject.objects.filter(uuid__in=subject_uuid)
        return subject

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['class_id'] = self.kwargs['id']
        return context


class TopicListView(ListView):

    model = Topic
    paginate_by = 100  # if pagination is desired

    def get_queryset(self, **kwargs):

        queryset = Topic.objects.filter(
            chapter__uuid=self.kwargs['chapter_id'], is_exercise=False).order_by('order_no')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chapter_id'] = self.kwargs['chapter_id']
        context['exercises'] = Topic.objects.filter(
            chapter=self.kwargs['chapter_id'], is_exercise=True).order_by('order_no')
        return context


class TopicCreateView(View):
    form_class = TopicCreateForm

    def get(self, request, chapter_id):
        order_no = Topic.objects.filter(chapter__uuid=chapter_id).aggregate(Max('order_no'))
        try:
            order_no = order_no['order_no__max'] + 1
        except TypeError:
            order_no = 1
        initial = {"chapter": chapter_id, "order_no": order_no}

        form = self.form_class(initial=initial)
        topics = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False).order_by('-created_at')
        form.fields["topic"].queryset = topics
        return render(request, 'stcapp/topic_form.html', {'form': form})

    def post(self, request, chapter_id):
        form = self.form_class(request.POST)
        if form.is_valid:
            form.save()
            return redirect('topic-list-view', chapter_id=chapter_id)
        else:
            return render(request, 'stcapp/topic_form.html', {'form': form})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chapter_id = self.kwargs['chapter_id']
        context['topics'] = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False)
        return context


class TopicUpdateView(UpdateView):
    model = Topic
    fields = '__all__'

    def get_object(self):
        return Topic.objects.get(uuid=self.kwargs.get('topic_id'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chapter_id = self.kwargs['chapter_id']
        context['topics'] = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False)
        return context

    def get_success_url(self):
        return reverse('topic-list-view', kwargs={'chapter_id': self.kwargs['chapter_id']})


class TopicDeleteView(DeleteView):
    model = Topic

    def get_success_url(self):
        return reverse('topic-list-view', kwargs={'chapter_id': self.kwargs['chapter_id']})


# -------------------------------------------------------------------------------


class ExerciseCreateView(View):
    form_class = TopicCreateForm

    def get(self, request, chapter_id):
        order_no = Topic.objects.filter(chapter__uuid=chapter_id).aggregate(Max('order_no'))
        try:
            order_no = order_no['order_no__max'] + 1
        except TypeError:
            order_no = 1
        initial = {"chapter": chapter_id, "order_no": order_no, 'is_exercise': True}

        form = self.form_class(initial=initial)
        topics = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False,
                                      is_exercise=True).order_by('-created_at')
        form.fields["topic"].queryset = topics
        return render(request, 'stcapp/topic_form.html', {'form': form})

    def post(self, request, chapter_id):
        form = self.form_class(request.POST)
        if form.is_valid:
            form.save()
            return redirect('topic-list-view', chapter_id=chapter_id)
        else:
            return render(request, 'stcapp/topic_form.html', {'form': form})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chapter_id = self.kwargs['chapter_id']
        context['topics'] = Topic.objects.filter(chapter__uuid=chapter_id,
                                                 is_sub_topic=False, is_exercise=True)
        return context


class ExerciseUpdateView(UpdateView):
    model = Topic
    fields = '__all__'

    def get_object(self):
        return Topic.objects.get(uuid=self.kwargs.get('topic_id'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chapter_id = self.kwargs['chapter_id']
        context['topics'] = Topic.objects.filter(chapter__uuid=chapter_id,
                                                 is_sub_topic=True, is_exercise=True)
        return context

    def get_success_url(self):
        return reverse('topic-list-view', kwargs={'chapter_id': self.kwargs['chapter_id']})


class QuestionAnswer(View):
    form_class = QuestionAnswerForm
    template_name = 'stcapp/question_answer_template.html'

    def get(self, request, chapter_id, topic_id):
        # try:
        topic = Topic.objects.get(uuid=topic_id)
        question_answer = Question.objects.filter(topic__uuid=topic_id)
        question_form = self.form_class(initial={'topic': topic})
        # except Topic.DoesNotExist:
        #     exercise = Exercise.objects.get(uuid=topic_id)
        #     question_answer = Question.objects.filter(exercise__uuid=topic_id)
        #     question_form = self.form_class(initial={'exercise': exercise})
        if question_answer:
            form = self.form_class(instance=question_answer.first())
        else:
            form = question_form
        return render(request, self.template_name, {'form': form})

    def post(self, request, chapter_id, topic_id):
        form = self.form_class(request.POST)
        question_answer = Question.objects.filter(topic__uuid=topic_id)
        # if not question_answer:
        #     question_answer = Question.objects.filter(exercise__uuid=topic_id)
        if question_answer:
            form = self.form_class(request.POST, instance=question_answer.first())
        if form.is_valid():
            form_instance = form.save(commit=False)
            if form_instance.video_key:
                url_len = form_instance.video_key.split('/')
                if form_instance.video_key and len(url_len) > 3:
                    video_id = form_instance.video_key.split('/')[-2]
                    video_h = form_instance.video_key.split('/')[-1]
                    video_key = f'{video_id}?h={video_h}'
                    form_instance.video_key = video_key
            form_instance.save()
            return redirect('topic-list-view', chapter_id=chapter_id)

        return render(request, self.template_name, {'form': form})


class UploadVideo(View):
    def get(self, request, topic_id):
        # try:
        topic = Topic.objects.get(uuid=topic_id)
        video_title = topic.video_title
        # except
        subject = topic.chapter.subject
        class_name = topic.chapter.class_name
        teacher = Teachers.objects.get(subject=subject, class_name=class_name)
        if not video_title:
            return HttpResponse('video title is Empty in this Topic.Please add video title')
        base_url = request.get_host()
        form = ManageVimeo().get_upload_video_form(video_title, str(topic.uuid), teacher.folder_id, base_url)
        return render(request, 'stcapp/upload_video_form.html',
                      {'form': form, 'topic': topic})


class ViewUploadedVideo(View):
    def get(self, request, topic_id, folder_id):
        video_uri = request.GET.get('video_uri')
        video_id = video_uri.split('/')[-1].split(':')[0]
        ManageVimeo().move_video(folder_id, video_id)
        video_url = f'{settings.VIMEO_BASE_URL}{video_id}'
        return render(request, 'stcapp/view_uploaded_video.html', {'video_url': video_url})


class ChapterCreateView(CreateView):
    model = Chapter
    fields = ['name', 'chapter_no', 'subject', 'class_name']

    def get_success_url(self):
        return reverse('chapter-list', kwargs={'subject_id': self.kwargs['subject_id'],
                                               'class_id': self.kwargs['class_id']})


class ChapterUpdateView(UpdateView):
    model = Chapter
    fields = ['name', 'chapter_no', 'subject', 'class_name']

    def get_success_url(self):
        return reverse('chapter-list', kwargs={'subject_id': self.kwargs['subject_id'],
                                               'class_id': self.kwargs['class_id']})


class ChapterDeleteView(DeleteView):
    model = Chapter

    def get_success_url(self):
        return reverse('chapter-list', kwargs={'subject_id': self.kwargs['subject_id'],
                                               'class_id': self.kwargs['class_id']})
