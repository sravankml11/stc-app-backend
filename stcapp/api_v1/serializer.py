from accounts.models import ClassName
from django.conf import settings
# from django.db import models
from rest_framework import serializers
# from stcapp.handler.aws_services import S3Objects
from stcapp.models import Chapter  # Exercise,
from stcapp.models import (Question, Subject, Teachers, Topic, VideoAction,
                           VideoWatched)


class SubjectSerializer(serializers.ModelSerializer):
    subject_choice = serializers.SerializerMethodField()

    def get_subject_choice(self, obj):
        return obj.subject_choice.value

    class Meta:
        model = Subject
        fields = '__all__'


class TeacherSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer()

    class Meta:
        model = Teachers
        fields = '__all__'


class ChapterSerializer(serializers.ModelSerializer):
    # subject = SubjectSerializer()

    class Meta:
        model = Chapter
        fields = '__all__'


class TopicSerializer(serializers.ModelSerializer):

    class Meta:
        model = Topic
        fields = '__all__'


# class ExercisesSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Exercise
#         fields = '__all__'


class VideoActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoAction
        fields = '__all__'


class QuestionAnswersSerializer(serializers.ModelSerializer):
    topic = TopicSerializer()
    sub_topic = serializers.SerializerMethodField()
    video = serializers.SerializerMethodField()
    # exercise = ExercisesSerializer()

    def get_video(self, obj):
        # import requests

        # url = "https://vimeo.com/api/oembed.json"

        # querystring = {"url": "https://vimeo.com/562485493/4a3c18ab8c"}

        # headers = {
        #     'cache-control': "no-cache",
        #     'postman-token': "caea1e55-483d-b7c2-cab2-4a40120fea37"
        # }

        # response = requests.request("GET", url, headers=headers, params=querystring).json()

        # return response.get('html')
        # if obj.video_key:
        #     url = S3Objects().get_object_url(key=obj.video_key)
        return f'{settings.VIMEO_BASE_URL}{obj.video_key}'
        # return obj.video_key

    def get_sub_topic(self, obj):
        sub = obj.topic.sub_topic.all().order_by('order_no')
        data = TopicSerializer(sub, many=True).data
        return data

    class Meta:
        model = Question
        fields = '__all__'


class VideoWatchedSerializer(serializers.ModelSerializer):

    class Meta:
        model = VideoWatched
        fields = '__all__'


class CheckLaterVideoActionSerializers(serializers.ModelSerializer):
    topic = TopicSerializer()

    class Meta:
        model = VideoAction
        fields = '__all__'


class ClassNameSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    division = serializers.SerializerMethodField()

    def get_name(self, obj):

        return obj.name.value

    def get_division(self, obj):
        return obj.division.value

    class Meta:
        model = ClassName
        fields = '__all__'
