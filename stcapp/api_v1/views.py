from accounts.models import ClassName
from django.db.models import Count
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from stcapp.api_v1.serializer import ChapterSerializer  # ExercisesSerializer,
from stcapp.api_v1.serializer import (  # VideoWatchedSerializer
    CheckLaterVideoActionSerializers, ClassNameSerializer,
    QuestionAnswersSerializer, SubjectSerializer, TopicSerializer,
    VideoActionSerializer)
from stcapp.models import Chapter  # Exercise,
from stcapp.models import (ProfileDetails, Question, Subject, Teachers, Topic,
                           VideoAction, VideoWatched)


class ChaptersView(generics.ListAPIView):

    queryset = Chapter.objects.all().order_by('chapter_no')
    serializer_class = ChapterSerializer

    def get_queryset(self):
        user = self.request.user
        subject_choice = self.request.GET.get('subject')
        subject = Subject.objects.get(subject_choice=subject_choice)
        chapter = Chapter.objects.filter(
            subject=subject, class_name=user.profile.class_name).order_by('chapter_no')
        return chapter


class TopicView(generics.ListAPIView):
    serializer_class = TopicSerializer

    def get_queryset(self):
        chapter_id = self.request.GET.get('chapter_id')
        topics = Topic.objects.filter(chapter__uuid=chapter_id, is_sub_topic=False,
                                      is_accessible=False).order_by('order_no')
        return topics


class ExercisesView(generics.ListAPIView):
    serializer_class = TopicSerializer

    def get_queryset(self):
        chapter_id = self.request.GET.get('chapter_id')
        topics = Topic.objects.filter(chapter__uuid=chapter_id,
                                      is_sub_topic=False, is_exercise=True).order_by('order_no')
        return topics


class QuestionAnswersView(APIView):
    serializer_class = QuestionAnswersSerializer

    def get(self, request):
        user = request.user
        topic_id = request.GET.get('topic_id')

        answer = Question.objects.filter(topic__uuid=topic_id)
        topic = answer.first().topic
        video_watched_obj, created = VideoWatched.objects.update_or_create(
            user=user, topic=topic, defaults={'user': user, 'topic': topic})
        answer_data = QuestionAnswersSerializer(answer, many=True).data
        try:
            video_action = VideoAction.objects.get(user=user, topic=topic)
            video_action_data = VideoActionSerializer(video_action).data

            answer_data[0]['video_action'] = video_action_data
        except Exception:
            answer_data[0]['video_action'] = None

        return Response(answer_data)


class SubjectView(generics.ListAPIView):
    serializer_class = SubjectSerializer
    queryset = Subject.objects.all()

    def get_queryset(self):
        user = self.request.user
        profile_details = ProfileDetails.objects.get(profile=user)
        teachers = Teachers.objects.filter(class_name=profile_details.class_name, active=True)
        subject_uuid = teachers.values_list('subject', flat=True)
        subject = Subject.objects.filter(uuid__in=subject_uuid)
        return subject


class VideoActionView(APIView):

    def post(self, request):
        data = request.data
        question = data.get('question')
        user = request.user
        check_later = data.get('check_later', False)
        got_it = data.get('got_it', False)
        not_clear = data.get('not_clear', False)

        question = Question.objects.get(uuid=question)

        video_action, created = VideoAction.objects.get_or_create(user=user,
                                                                  topic=question.topic)
        video_action.check_later = check_later
        video_action.got_it = got_it
        video_action.not_clear = not_clear
        video_action.save()
        return Response(status=status.HTTP_201_CREATED)


class CheckLaterAPI(APIView):
    def get(self, request):
        user = request.user
        subject_choice = request.GET.get('subject')
        video_actions = VideoAction.objects.filter(
            user=user, check_later=True,
            topic__chapter__subject__subject_choice=subject_choice).order_by('-modified_at')
        # import pdb
        # pdb.set_trace()
        # topic = Topic.objects.filter(uuid__in=video_actions.values_list(
        #     'topic', flat=True), chapter__subject__subject_choice=subject_choice)
        # # exercise = Exercise.objects.filter(uuid__in=video_actions.values_list(
        # #     'question__exercise', flat=True), chapter__subject__subject_choice=subject_choice)
        topic_data = CheckLaterVideoActionSerializers(video_actions, many=True).data
        # exercise_data = ExercisesSerializer(exercise.order_by('-modified_at'), many=True).data
        return Response({'topic': topic_data,
                         #  'exercise': exercise_data
                         })


class GetVideoWatchAPI(APIView):

    def get(self, request):
        data = request.GET
        subject = data.get('subject')
        class_uuid = data.get('class_uuid')

        video_watched_obj = VideoWatched.objects.filter(
            topic__chapter__subject__subject_choice=subject,
            topic__chapter__class_name__uuid=class_uuid)
        data = video_watched_obj.values('user__first_name', 'user__last_name').annotate(
            watched=Count('topic')).order_by('-watched')
        return Response(list(data))

#     def post(self, request, *args, **kwargs):
#         user = request.user
#         data = request.data
#         topic = data.get('topic')
#         question = Question.objects.get(Q(topic__uuid=topic) | Q(exercise__uuid=topic))
#         video_watched_obj, created = VideoWatched.objects.update_or_create(
#             user=user, question=question, defaults={'user': user, 'question': question})
#         # video_watched_obj.watch_count = video_watched_obj.watch_count + 1
#         # video_watched_obj.save()
#         return Response(status=status.HTTP_201_CREATED)


class VideoActionStats(APIView):
    def get(self, request):
        data = request.GET
        subject = data.get('subject')
        class_uuid = data.get('class_uuid')
        teacher = data.get('teacher')
        user = request.user
        not_clear = False
        got_it = False
        if data.get('not_clear', False):
            not_clear = True
        else:
            got_it = True

        video_action = VideoAction.objects.filter(
            topic__chapter__subject__subject_choice=subject,
            topic__chapter__class_name__uuid=class_uuid,
            not_clear=not_clear, got_it=got_it)
        if not teacher:
            video_action = video_action.filter(user=user)

        data = video_action.values('topic__name', 'topic__uuid', 'topic__chapter').annotate(
            count=Count('topic')).order_by('-count')
        return Response(list(data))


class AllowVideoAcessable(APIView):
    def get(self, request, topic):

        topic_id = topic
        topic = Topic.objects.get(uuid=topic_id)
        sub_topic = Topic.objects.filter(topic=topic)
        if not topic.is_accessible:
            topic.is_accessible = True
            topic.save()
            sub_topic.update(is_accessible=True)
        else:
            topic.is_accessible = False
            topic.save()
            sub_topic.update(is_accessible=False)

        return Response(status=status.HTTP_202_ACCEPTED)


class GetStudentVideoAction(APIView):

    def get(self, request):
        data = request.GET
        got_it = data.get('got_it')
        not_clear = data.get('not_clear')
        topic = data.get('topic')
        if not_clear:
            students = VideoAction.objects.filter(not_clear=True, topic__uuid=topic)
        if got_it:
            students = VideoAction.objects.filter(got_it=True, topic__uuid=topic)

        students_list = students.values_list('user__first_name', flat=True)

        return Response(students_list)


class GetTeachersClassList(APIView):

    def get(self, request):
        user = request.user
        teachers = Teachers.objects.filter(user=user).values_list('class_name', flat=True)
        class_list = ClassName.objects.filter(uuid__in=teachers)
        class_name_data = ClassNameSerializer(class_list, many=True).data
        return Response(class_name_data)
