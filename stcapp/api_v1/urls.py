from django.urls import path
from stcapp.api_v1.views import (AllowVideoAcessable, ChaptersView,
                                 CheckLaterAPI, ExercisesView,
                                 GetStudentVideoAction, GetTeachersClassList,
                                 GetVideoWatchAPI, QuestionAnswersView,
                                 SubjectView, TopicView, VideoActionStats,
                                 VideoActionView)

urlpatterns = [
    path('chapter/list/', ChaptersView.as_view(), name='chapter-list'),
    path('chapter/topic/list/', TopicView.as_view(), name='topic-list'),
    path('chapter/exercises/list/', ExercisesView.as_view(), name='exercises-list'),
    path('chapter/topic/answer/', QuestionAnswersView.as_view(), name='topic-list'),
    path('subject/list/', SubjectView.as_view(), name='subject-list'),
    path('create/video/action/', VideoActionView.as_view(), name='video-action'),
    path('check/later/list/', CheckLaterAPI.as_view(), name='check-later-list'),
    path('video/watched/list/', GetVideoWatchAPI.as_view(), name='video-watched'),
    path('video/action/stats/', VideoActionStats.as_view(), name='video-action-stats'),
    path('video/<uuid:topic>/accessible/', AllowVideoAcessable.as_view(), name='video-accessible'),
    path('student/video/action/get/', GetStudentVideoAction.as_view(), name='video-action-list'),
    path('teacher/class/list/', GetTeachersClassList.as_view(), name='teacher-class-list'),

]
