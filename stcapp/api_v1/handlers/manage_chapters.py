from stcapp.api_v1.serializer import ChapterSerializer
from stcapp.models import Chapter


class ManageChapters:
    def get_chapter_list(self):
        chapter_list = Chapter.objects.all().order_by('chapter_no')
        return ChapterSerializer(chapter_list, many=True).data
