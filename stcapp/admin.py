from accounts.models import ClassName, School
from django.contrib import admin

from stcapp.models import (Chapter, ProfileDetails, Question, Subject,
                           Teachers, Topic, VideoAction, VideoWatched)

admin.site.register(School)
admin.site.register(Topic)
admin.site.register(Subject)
admin.site.register(Chapter)
admin.site.register(ProfileDetails)
admin.site.register(Question)
admin.site.register(Teachers)
admin.site.register(ClassName)
admin.site.register(VideoAction)
admin.site.register(VideoWatched)
