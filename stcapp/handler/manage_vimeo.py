import os

import vimeo


class VimeoAuth:
    def __init__(self):
        self.client = vimeo.VimeoClient(
            token=os.getenv('VIMEO_TOKEN'),
            key=os.getenv('VIMEO_KEY'),
            secret=os.getenv('VIMEO_SECRET'))


class ManageVimeo(VimeoAuth):

    def get_upload_video_form(self, video_title, topic_uuid, folder_id, base_url):
        response = self.client.post('https://api.vimeo.com/me/videos',
                                    data={"upload": {"approach": "post",
                                                     "redirect_url": f"http://{base_url}/dashboard/upload/video/view/{topic_uuid}/{folder_id}/"},  # noqa
                                          "name": video_title},)
        return response.json()['upload']['form']

    def move_video(self, folder_id, video_id):
        uri = f'/me/projects/{folder_id}/videos/{video_id}'
        self.client.put(uri)
        return True
