import boto3
from django.conf import settings


class S3Objects:
    def __init__(self):
        self.client = boto3.client('s3')

    def get_object_url(self, key):
        url = self.client.generate_presigned_url(ClientMethod='get_object',
                                                 Params={'Bucket': settings.AWS_S3_BUCKET_NAME,
                                                         'Key': key},
                                                 HttpMethod="GET",
                                                 ExpiresIn=10800,)
        return url
