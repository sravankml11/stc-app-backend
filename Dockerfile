FROM python:3.7

EXPOSE 3000

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ENV DJANGO_ENV=${DJANGO_ENV} \
  DJANGO_SETTINGS_MODULE=settings.local\
  DB_NAME=stcapp\
  DB_USER=docker\
  DB_PASSWORD=docker\
  DB_HOST=databse\
  DB_PORT=5432\
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}\
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}\
  VIMEO_TOKEN=${VIMEO_TOKEN}\
  VIMEO_KEY=${VIMEO_KEY}\
  VIMEO_SECRET=${VIMEO_SECRET}




# Project initialization:

# RUN pip3 install --no-cache-dir -U pip pipenv 

# COPY ./Pipfile .
# COPY ./Pipfile.lock .

COPY ./requirements.txt .
# -- Install dependencies:
RUN pip install -r requirements.txt
# RUN pipenv install --system


COPY . .
